package ru.t1.panasyuk.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.dto.Domain;
import ru.t1.panasyuk.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    public AbstractDataCommand() {
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(getServiceLocator().getProjectService().findAll());
        domain.setTasks(getServiceLocator().getTaskService().findAll());
        domain.setUsers(getServiceLocator().getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        getServiceLocator().getUserService().set(domain.getUsers());
        getServiceLocator().getProjectService().set(domain.getProjects());
        getServiceLocator().getTaskService().set(domain.getTasks());
        getServiceLocator().getAuthService().logout();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
