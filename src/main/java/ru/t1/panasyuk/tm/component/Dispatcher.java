package ru.t1.panasyuk.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.endpoint.Operation;
import ru.t1.panasyuk.tm.dto.request.AbstractRequest;
import ru.t1.panasyuk.tm.dto.response.AbstractResponse;
import ru.t1.panasyuk.tm.exception.system.RequestNotSupportedException;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        if (operation == null) throw new RequestNotSupportedException(request.getClass().getName());
        return operation.execute(request);
    }

}